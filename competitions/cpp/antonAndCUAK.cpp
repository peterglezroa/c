/*B. Anton and currency you all know -> http://codeforces.com/problemset/problem/508/B*/
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;
int main(){
  int pos = 0, aux = 0;
  string num;
  cin >> num;
  int lastPos = num.length()-1, changePos = -1;
  if(num[lastPos]%2 != 0){
    while (pos < lastPos) {
      if(num[pos]%2 == 0){
        changePos = pos;
        if(num[lastPos] > num[pos]){
          break;
        }
      }
      pos++;
    }
  }else{
    while (pos < lastPos) {
      if(num[pos]%2 != 0){
        changePos = pos;
        if(num[lastPos] > num[pos]){
          break;
        }
      }
      pos++;
    }
  }
  if(changePos == -1){
    printf("-1");
  }else{
    aux = num[lastPos];
    num[lastPos] = num[changePos];
    num[changePos] = aux;
    cout << num;
  }
  return 0;
}
