/*A. Arrival of the General -> http://codeforces.com/problemset/problem/144/A*/
#include <stdio.h>

int main(){
  int cant, biggest, posB = 0, smallest, posS = 0, aux;
  scanf("%i", &cant);
  scanf("%i", &biggest);
  smallest = biggest;
  for (int i = 1; i < cant; i++) {
    /* code */
    scanf("%i", &aux);
    if(biggest < aux){
      posB = i;
      biggest = aux;
    }else if(smallest >= aux){
      posS = i;
      smallest = aux;
    }
  }
  aux = (cant - 1 - posS) + posB;
  if (posS < posB)
    aux--;
  printf("%i", aux);
  return 0;
}
