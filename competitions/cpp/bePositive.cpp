/*https://vjudge.net/contest/338364#problem/B*/
#include <bits/stdc++.h>
using namespace std;
int main(){
   int cant, pos = 0, cer = 0, neg = 0;
   scanf("%i", &cant);
   int num[cant];
   for (int i = 0; i < cant; i++) {
      scanf("%i", &num[i]);
      if(num[i] < 0)
         neg++;
      else if(num[i] == 0)
         cer++;
      else
         pos++;
   }
   if(pos >= neg){
		if(pos >= ceil(cant/2.0)){
			cout << 1;
		} else {
			cout << 0;
		}
	} else {
		if(neg >= ceil(cant/2.0)){
			cout << -1;
		} else {
			cout << 0;
		}
	}
   return 0;
}
