/*B.Cells Not Under Attack -> http://codeforces.com/problemset/problem/701/B*/
#include<stdio.h>
int main(){
   int size, rooks, posr, posc;
   scanf("%d %d", &size, &rooks);
   bool rows[size];
   bool cols[size];
   long long unsigned int rowCount = size, colCount = size;
   for (size_t i = 0; i < size; i++) {
      rows[i] = false;
      cols[i] = false;
   }
   for (size_t i = 0; i < rooks; i++) {
      scanf("%i %i", &posr, &posc);
      posr--;
      posc--;
      if(rows[posr] == false){
         rows[posr] = true;
         rowCount--;
      }
      if(cols[posc] == false){
         cols[posc] = true;
         colCount--;
      }
      printf("%lli ", colCount*rowCount);
   }
   return 0;
}
