#include <bits/stdc++.h>
using namespace std;
int main(){
   bool change = false;
   int r, aux;
   array<int,3> m;
   cin >> r >> m[0] >> m[1] >> m[2];
   sort(m.begin(), m.end());
   int dp[r+1] = { 0 };
   for(int j = 0; j < 3; j++){
      if(m[j] <= r)
         dp[m[j]] = 1;
   }
   for(int i = 0; i <= r; i++){
      if(i-m[0] < 0)
         dp[i] = 0;
      else{
         change = false;
         for(int j = 0; j < 3; j++){
            if(i-m[j] >= 0)
               if(dp[i] <= dp[i-m[j]]){
                  change = true;
                  dp[i] = dp[i-m[j]];
               }
         }
         if(dp[i] > 0 && change)
            dp[i]++;
      }
   }
   cout << dp[r];
   return 0;
}
