#include <bits/stdc++.h>
using namespace std;
int main(){
   int s;
   string num1, num2;
   cin >> num1 >> num2;
   char num3[num2.length()] = {'0'};
   bool digits[10] = {false};
   for (int i = 0; i < num2.length(); i++) {
      s = num2[i]-48;
      if(digits[s]){
         while(s < 10){
            if(!digits[s])
               break;
            s++;
         }
         if(s >= 10)
            break;
      }
      num3[i] = num2[i];
      digits[s] = true;
   }
   cout << num3;
   return 0;
}
