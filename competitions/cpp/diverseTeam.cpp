#include <bits/stdc++.h>
typedef long long int ll;
using namespace std;
int main(){
	int n, k, aux, i;
	unordered_map<int, int> mapa;
	unordered_map<int,int>::iterator it;
	cin >> n >> k;
 	for(i = 0; i < n; i++){
		cin >> aux;
		mapa[aux] = i+1;
	}
	i = 0;
	it = mapa.begin();
	if(mapa.size() >= k){
		cout << "YES" << endl;
		while(i < k){
			cout << (*it).second << " ";
			it++;
			i++;
		}
	} else {
		cout << "NO";
	}
	return 0;
}
