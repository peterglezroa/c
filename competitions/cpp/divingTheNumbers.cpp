// https://codeforces.com/problemset/problem/899/C
#include <stdio.h>
#include <math.h>
#include <string>
#include <iostream>

using namespace std;
int main(int argc, char *argv[]){
    int n, count, amount;
    string g = "";
    scanf("%i", &n);
    (n%4 == 0 || n%4 == 3)? printf("0\n"):printf("1\n");
    (n%2 == 0)? count = 1:count = 2;
    amount = n/2;
    printf("%i ", amount);
    for(int i = 0; i < amount; i++) {
        if(i%2 != 0) {
            printf("%i ", count);
            count += 2;
        } else {
            printf("%i ", n);
            n -= 2;
        }
    }
    return 0;
}
