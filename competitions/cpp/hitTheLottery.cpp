#include <bits/stdc++.h>
using namespace std;
typedef unsigned int ui;
int main(){
   ui bills[5] = {100,20,10,5,1};
   ui money;
   cin >> money;
   int point = 0, count = 0;
   while(money > 0){
      if(money < bills[point])
         point++;
      else{
         money -= bills[point];
         count++;
      }
   }
   cout << count;
   return 0;
}
