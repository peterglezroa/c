/*C. Kuro And Walking Route -> http://codeforces.com/problemset/problem/979/C*/
#include <stdio.h>
#include <iostream>
#include <stack>
using namespace std;
int findPath(int **dp, int size, int start, int dest, int flower, int bee){
   if(start == dest)
      return 0;
   bool flowered;
   int actual;
   stack<int> visit;
   int visited[size] = { 0 };
   visit.push(start);
   while(!visit.empty()){
      actual = visit.top();
      visit.pop();
      if(actual == flower)
         flowered = true;
      for (int i = 0; i < size; i++) {
         if(dp[actual][i]){
            if(i == dest && !(flowered && dest == bee))
               return 1;
            else if(i == dest)
               return 2;
            else{
               if(!visited[i]){
                  visited[i] = 1;
                  visit.push(i);
               }
            }
         }
      }
   }
   return 0;
}
int main(){
   int size, bee, flower, a, b, count = 0, ptm = 0;
   scanf("%i %i %i", &size, &flower, &bee);
   flower--;bee--;
   int *dp[size];
   for (int i = 0; i < size; i++) {
      dp[i] = new int[10];
      for (int j = 0; j < size; j++) {
         dp[i][j] = 0;
      }
   }
   for (int i = 0; i < size-1; i++) {
      scanf("%i %i", &a, &b);
      a--;b--;
      dp[a][b] = 1;
      dp[b][a] = 1;
   }
   for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
         dp[i][j] = findPath(dp,size,i,j, flower, bee);
         if(dp[i][j]){
            printf("%i %i %i\n", i,j,dp[i][j]);
            cout << count << endl;
            if(dp[i][j] == 1){
               count++;
            }
            dp[j][i] = dp[i][j];
         }
      }
   }
   printf("%i", count);
   return 0;
}
