/*C. LuckyNumbers -> https://codeforces.com/problemset/problem/630/C*/
#include <iostream>
#include <stdio.h>
using namespace std;
unsigned long long int fastPow(unsigned long long int x, int n){
   if(n<0)
      return fastPow(1/x, -n);
   else if(n==0)
      return 1;
   else if(n==1)
      return x;
   else if(n%2==0)
      return fastPow(x*x, n/2);
   else
      return x*fastPow(x*x, (n-1)/2);
}
int main(){
   int n;
   scanf("%i", &n);
   printf("%llu", fastPow(2,n+1)-2);
   return 0;
}
