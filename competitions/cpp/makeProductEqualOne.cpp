#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long int ll;
int main(){
   int n, negs = 0,aux, ceros = 0;
   ll moves = 0;
   cin >> n;
   for(int i = 0; i < n; i++){
      cin >> aux;
      if(aux < 0)
         negs++;
      if(aux == 0)
         ceros++;
      moves += abs(abs(aux) - 1);
   }
   if(negs%2 == 0 || ceros)
      cout << moves;
   else
      cout << moves + 2;

   return 0;
}
