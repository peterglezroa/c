#include <bits/stdc++.h>
using namespace std;
int main(){
   int n, count = 1, aux, prev, mx = 0;
   cin >> n;
   cin >> prev;
   for(int i = 1; i < n; i++){
      cin >> aux;
      if(prev >= aux){
         mx=max(count, mx);
         count = 1;
      }else
         count++;
      prev = aux;
   }
   mx=max(count,mx);
   cout << mx;
   return 0;
}
