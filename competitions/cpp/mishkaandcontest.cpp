#include <bits/stdc++.h>
typedef long long int ll;
using namespace std;
int main(){
	int cant, maxd, left, right, count = 0;
	scanf("%i %i", &cant, &maxd);
	int problems[cant];
	for(int i = 0; i < cant; i++){
		cin >> problems[i];
	}
	right = cant -1; left = 0;
	while(left <= right){
		if(problems[left] <= maxd){
			count++;
		} else {
			break;
		}
		left++;
	}
	while(right > left){
		if(problems[right] <= maxd){
			count++;
		} else {
			break;
		}
		right--;
	}
	cout << count;
	return 0;
}
