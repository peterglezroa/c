#include <bits/stdc++.h>
typedef long long int ll;
using namespace std;
int main(){
	int n, m, odd = 0;
	cin >> n >> m;
	vector<int> v(m);
	for(int i = 0; i < m; i++){
		cin >> v[i];
	}
	if(n % 2 == 0){
		if(v[m - 1] % 2 == 0){
			cout << "even";
		} else {
			cout << "odd";
		}
	}else{
		for(int i = 0; i < m; i++){
			if(v[i] % 2 == 0){
				//Do nothing
			} else {
				odd++;
			}
		}
		if(odd % 2 == 0){
			cout << "even";
		} else {
			cout << "odd";
		}
	}
	return 0;
}
