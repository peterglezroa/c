/*A. Presents -> http://codeforces.com/problemset/problem/136/A*/
#include <stdio.h>
using namespace std;

int main(){
   int cant, aux;
   scanf("%i", &cant);
   int gifts[cant];
   for (size_t i = 0; i < cant; i++) {
      scanf("%i", &aux);
      gifts[aux - 1] = i + 1;
   }
   for (size_t i = 0; i < cant; i++) {
      printf("%i ", gifts[i]);
   }
   return 0;
}
