/*https://vjudge.net/contest/338364#problem/H*/
#include <bits/stdc++.h>
typedef long long int ll;
using namespace std;
int main(){
   int cant;
   ll a,b, max;
   scanf("%i",&cant);
   ll tests[cant];
   vector<int> tested(cant, 0);
   for (int i = 0; i < cant; i++) {
      tested[i] = i;
      scanf("%lld %lld", &a, &b);
      tests[i] = a-b;
      if(tests[i] > max)
         max = tests[i];
   }
   bool primes[max/2] = {true};
   ll j;
   for (ll i = 2; i < max/2; i++) {
      if(primes[i]){
         j = i;
         for (int i = 0; i < tested.size(); i++) {
            if(tests[tested[i]]%primes[i] == 0)
               tested.erase(tested.begin() + i);
         }
         if(tested.empty())
            break;
         while(j <= max/2){
            primes[j] = false;
            j+=i;
         }
      }
   }
   return 0;
}
