/*Queries about less or equal elements -> http://codeforces.com/problemset/problem/600/B*/
#include <stdio.h>
#include <algorithm>
using namespace std;

int binarySearch(int v[], int elem, int size){
   int low = 0, high = size - 1, mid = (low+high)/2;
   while(low <= high){
      if(v[mid] <= elem){
         low = mid +1;
      }else{
         high = mid-1;
      }
      mid = (low+high)/2;
   }
   return low;
}
int main() {
   int a_size, b_size, b, pos;
   scanf("%i %i", &a_size, &b_size);
   int a[a_size];
   for (int i = 0; i < a_size; i++){
      scanf("%i", &a[i]);
   }
   sort(a, a + a_size);
   for (int i = 0; i < b_size; i++) {
      scanf("%i", &b);
      printf("%i ", binarySearch(a,b, a_size));
   }
   return 0;
}
