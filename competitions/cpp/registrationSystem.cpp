/*Registration System -> http://codeforces.com/problemset/problem/4/C*/
#include <iostream>
#include <vector>
#include <string>
using namespace std;
int main() {
   vector<string> users;
   vector<int> cant;
   int user_cant;
   string user;
   cin >> user_cant;
   for(int i = 0; i < user_cant; i++){
      bool found = false;
      cin >> user;
      for (int j = 0; j < users.size(); j++) {
         if(users[j].length() == user.length()){ //Existe en la base de datos
            int pointer = 0;
            while (pointer < user.length() && users[j][pointer] == user[pointer]) {
               pointer++;
            }
            if(pointer == user.length()){
               cant[j]++;
               cout << users[j] << cant[j] << endl;
               found = true;
               break;
            }
         }
      }
      if(!found){
         users.push_back(user);
         cant.push_back(0);
         cout << "OK" << endl;
      }
   }
   return 0;
}
