/*Roadside Trees -> http://codeforces.com/contest/265/problem/B*/
#include <iostream>
#include <string>
using namespace std;
int main(){
  int tree_cant;
  scanf("%d", &tree_cant);
  int tree, count, current;
  cin >> current;
  count = current + 1;
  for(int i = 1; i < tree_cant; i++){
    cin >> tree;
    if(current > tree){
      count += (current-tree);
      current = tree;
    }
    count += (tree-current) + 2;
    current = tree;
  }
  cout << count;
  return 0;
}
