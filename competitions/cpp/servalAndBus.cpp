#include <bits/stdc++.h>
typedef long long int ll;
using namespace std;
int main(){
	int n, t, mini = 10000000, ind = 100000, aux;
	cin >> n >> t;
	vector<pair<int,int>> v(n);
	for(int i = 0; i < n; i++){
		cin >> v[i].first >> v[i].second;
	}
	for(int i = 0; i < n; i++){
		aux = v[i].first;
		while(aux < t){
			aux += v[i].second;
		}
		if(aux - t <= mini){mini = aux - t;ind = i + 1;
		}
	}
	cout << ind;
	return 0;
}
