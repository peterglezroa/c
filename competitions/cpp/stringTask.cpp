/*StringTask -> http://codeforces.com/problemset/problem/118/A*/
#include <iostream>
#include <string>
using namespace std;
bool vowel(char c){
  return (c == 'a' || c == 'o' || c == 'y' || c == 'e' || c == 'u' || c == 'i' || c == 'A' || c == 'O' || c == 'Y' || c == 'E' || c == 'U' || c == 'I');
}

int main(){
  std::string string;
  std::string res;
  cin >> string;
  for (int i = 0; i < string.length(); i++) {
    if(!vowel(string[i])){
      res += '.';
      if(string[i] <= 90 && string[i] >= 65){
        res += string[i] + 32;
      }else{
        res += string[i];
      }
    }
  }
  cout << res;

  return 0;
}
