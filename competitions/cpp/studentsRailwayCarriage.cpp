/*http://codeforces.com/problemset/problem/962/B*/
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <string>
using namespace std;
int main() {
   int n, a, b, estado = 0, grande, chico, count = 0;
   cin >> n >> a >> b;
   string student;
   cin >> student;
   if(a > b){
      grande = a;
      chico = b;
   }else{
      grande = b;
      chico = a;
   }
   for (int i = 0; i < student.length(); i++) {
      char c = student[i];
      if(c == '.'){
         if(estado == 0){
            if(grande > 0){
               grande--;
               count++;
            }
            estado = 1;
         }else{
            if(chico > 0){
               chico--;
               count++;
            }
            estado = 0;
         }
      }else{
         if (grande > chico)
            estado = 0;
         else
            estado = 1;
      }
   }
   cout << count;
   return 0;
}
