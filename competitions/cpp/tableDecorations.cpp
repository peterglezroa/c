/*C. Table Decorations -> http://codeforces.com/problemset/problem/478/C*/
#include <stdio.h>
#include <bits/stdc++.h>
using namespace std;
int main(){
    long long a[3];
    for(int i = 0; i < 3; i++){
        cin >> a[i];
    }
    sort(a,a+3);
    if(2*(a[0]+a[1]) <= a[2])
        cout << a[0]+a[1];
    else
        cout << (a[0]+a[1]+a[2])/3;
    return 0;
}
