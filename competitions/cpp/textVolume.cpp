#include<bits/stdc++.h>
using namespace std;
int main() {
  int len, max = 0, counter = 0;
  cin >> len;
  getchar();
  for (int i = 0; i < len; i++) {
    char c;
    c = getchar();
    if (isupper(c))
      counter++;
    if (isspace(c)) {
      if (counter > max)
        max = counter;
      counter = 0;
    }
  }
  if (counter > max)
    max = counter;
  cout << max;
  return 0;
}
