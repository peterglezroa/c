#include <bits/stdc++.h>
typedef long long int ll;
using namespace std;
int main(){
	ll n, x, y, distA, distB;
	pair<int,int> white, black;
	cin >> n;
	cin >> x >> y;
	white = make_pair(1,1);
	black = make_pair(n,n);
	distA = (x - 1) > (y - 1) ? (x - 1) : (y - 1);
	distB = (n - x) > (n - y) ? (n - x) : (n - y);
	if(distA < distB){
		cout << "White";
	}else if(distB == distA){
		cout << "White";
	}else{
		cout << "Black";
	}
	return 0;
}
