#include <bits/stdc++.h>
using namespace std;
int main(){
   int n, dis, aux, pos = 0, cp = 0, jumps = 0;
   string str;
   cin >> n >> dis >> str;
   for(int i = 0; i < n; i++){
      if(str[i] == '1'){
         if(i-pos > dis){
            if(cp <= pos)
               break;
            jumps++;
            pos = cp;
            if(i-cp <= dis)
               cp = i;
         }else
            cp = i;
      }
   }
   if(pos != n-1)
      if(cp != n-1)
         cout << "-1";
      else
         cout << jumps+1;
   else
      cout << jumps;
   return 0;
}
