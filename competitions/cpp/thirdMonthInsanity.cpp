// https://codeforces.com/problemset/problem/859/D
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]){
    int n, i, j, a;
    scanf("%i", &n);
    a = pow(2, n);
    float m[a][a], m2[a][a];
    for(i = 0; i < a; i++) {
        for(j = 0; j < a; j++) {
            scanf("%i", m[i][j]);
            m[i][j] /= 10;
        }
    }
    for(k = 0; k < a-1; k++) {
        for(i = 0; i < a; i++) {
            for(j = 0; j < a; j++) {
                for(l = 0; l < a; l++) {
                    for(m = 0; m < a; m++) {
                        m[i][j] /= 10;
                    }
                }
            }
        }
    }
}
