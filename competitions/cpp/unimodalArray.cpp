#include <bits/stdc++.h>
using namespace std;
int main(){
   int size, aux, prev = 0, state = 0, i;
   scanf("%i", &size);
   scanf("%i", &prev);
   for (i = 1; i < size; i++) {
      scanf("%i", &aux);
      if(state == 0){
         if(aux < prev)
            state = 3;
         if(aux == prev)
            state++;
      }else if(state == 1){
         if(aux < prev)
            state++;
         if(aux > prev)
            break;
      }else{
         if(aux >= prev)
            break;
      }
      prev = aux;
   }
   if(i == size)
      printf("YES");
   else
      printf("NO");
   return 0;
}
