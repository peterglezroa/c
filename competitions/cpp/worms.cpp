#include <bits/stdc++.h>
using namespace std;
int binarySearch(int *v, int low, int high, int elem){
   int mid = ceil(low + (high-low)/2);
   if(low <= high){
      if(v[mid] == elem){
         return mid;
      }
      if(v[mid] > elem){
         return binarySearch(v, low, mid - 1, elem);
      }
      return binarySearch(v, mid + 1, high, elem);
   }
   return -mid;
}
int main(){
   int size, aux, pointer;
   cin >> size;
   int piles[size];
   aux = 0;
   for(int i = 0; i < size; i++){
      cin >> piles[i];
      piles[i] += aux;
      aux = piles[i];
   }
   int cant;
   cin >> cant;
   for (int i = 0; i < cant; i++) {
      cin >> aux;
      pointer = abs(binarySearch(piles, 0, size-1, aux));
      if(i == cant -1)
         cout << pointer +1;
      else
         cout << pointer+1 << endl;
   }
}
