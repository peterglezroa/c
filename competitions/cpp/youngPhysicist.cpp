/*A. Young Physicist -> http://codeforces.com/problemset/problem/69/A*/
#include <stdio.h>
using namespace std;
int main(){
   int cant, x = 0, y = 0, z = 0, auxx = 0, auxy = 0, auxz = 0;
   scanf("%d", &cant);
   for(int i = 0; i < cant; i++){
      scanf("%d %d %d", &auxx, &auxy, &auxz);
      x += auxx;
      y += auxy;
      z += auxz;
   }
   if(x == 0 && y == 0 && z == 0){
      printf("YES");
   }else{
      printf("NO");
   }
   return 0;
}
