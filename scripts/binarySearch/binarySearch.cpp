/*Código para binary search de un vector*/
#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <iterator>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
using namespace std;

/*============================================================================================================*/
void display_menu(){
   cout << "<=======================Comandos=====================>" << endl;
   cout << "rndvect <n> => crea un vector de n tamano con numeros aleatorios" << endl;
   cout << "clearvect => borra el vector existente" << endl;
   cout << "dispvect => muestra el vector existente" << endl;
   cout << "push <elem> => empuja un valor al vector" << endl;
   cout << "find <elem> => busca un valor en el vector" << endl;
   cout << "find <elem> => busca un valor en el vector y compara los tiempos entre recursivo y ávido" << endl;
   cout << "exit => termina el programa" << endl;
   cout << "<======================/Comandos=====================>" << endl;
}

int binarySearch(vector<int> v, int low, int high, int elem){
   int mid = ceil(low + (high-low)/2);
   if(low <= high){
      if(v[mid] == elem){
         return mid;
      }
      if(v[mid] > elem){
         return binarySearch(v, low, mid - 1, elem);
      }
      return binarySearch(v, mid + 1, high, elem);
   }
   return -mid;
}
void mergeSort(vector<int>&left, vector<int>& right, vector<int>& v){
    int nL = left.size();
    int nR = right.size();
    int i = 0, j = 0, k = 0;
    while (j < nL && k < nR){
        if (left[j] < right[k]) {
            v[i] = left[j];
            j++;
        }
        else {
            v[i] = right[k];
            k++;
        }
        i++;
    }
    while (j < nL) {
        v[i] = left[j];
        j++; i++;
    }
    while (k < nR) {
        v[i] = right[k];
        k++; i++;
    }
}
void sort(vector<int>& v){
      if(v.size() <= 1)
         return;
      int mid = v.size()/2;
      vector<int> left;
      vector<int> right;
      for (size_t i = 0; i < mid;i++)
         left.push_back(v[i]);
      for (size_t i = mid; i < v.size(); i++)
         right.push_back(v[i]);
      sort(left);
      sort(right);
      mergeSort(left, right, v);
}
/*============================================================================================================*/
int main(){
   bool running = true;
   string cmd;
   int input;
   vector<int> v;
   vector<int>::iterator itr;
   int pos;


   auto start = chrono::high_resolution_clock::now();
   auto end = chrono::high_resolution_clock::now();
   double elapsed;
   ios_base::sync_with_stdio(false);
   while(running){
      cout << "-> ";
      cin >> cmd;
      if(cmd.compare("help") == 0){
         display_menu();
      }else if(cmd.compare("rndvect") == 0){
         cin >> input;
         srand (time(NULL));
         for (int i = 0; i < input; i++) {
            v.push_back(rand() % 100 + 1);
         }
         sort(v);
      }else if(cmd.compare("clearvect") == 0){
         if(!v.empty()){
            v.clear();
         }
      }else if(cmd.compare("dispvect") == 0){
         if(v.empty()){
            cout << "Vector vacío" << endl;
         }else{
            itr = v.begin();
            while (itr != v.end()) {
               cout << *itr << " ";
               itr++;
            }
            cout << endl;
         }
      }else if(cmd.compare("push") == 0){
         cin >> input;
         v.push_back(input);
         sort(v);
         //order(*v);
      }else if(cmd.compare("find") == 0){
         cin >> input;
         if(v.empty()){
            cout << "Vector vacío" << endl;
         }else{
            pos = binarySearch(v,0,v.size()-1,input);
            if(pos >= 0){
               cout << "Posición: " << pos << endl;
            }else{
               cout << "Elemento no encontrado" << pos <<endl;
            }
         }
      }else if(cmd.compare("findcomptimes") == 0){
         cin >> input;
         if(v.empty()){
            printf("Vector vacío\n");
         }else{
            start = chrono::high_resolution_clock::now();
            pos = binarySearch(v,0,v.size()-1,input);
            if(pos >= 0){
               printf("Pos: %i\n", pos);
            }else{
               printf("Elemento no encontrado, pos: %i\n", pos);
            }
            end = chrono::high_resolution_clock::now();
            elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
            elapsed *= 1e-9;
            cout << "Recursive: " << elapsed << " nanoseconds" << endl;
            start = chrono::high_resolution_clock::now();
            pos = binarySearch(v,0,v.size()-1,input);
            if(pos >= 0){
               printf("Pos: %i\n", pos);
            }else{
               printf("Elemento no encontrado, pos: %i\n", pos);
            }
            for (size_t i = 0; i < 100000; i++) {
               /* code */
            }
            end = chrono::high_resolution_clock::now();
            elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
            elapsed *= 1e-9;
            cout << "Avid: " << elapsed << " nanoseconds" <<endl;
         }
      }else if(cmd.compare("exit") == 0){
         running = false;
      }else{
         cout << "Comando no existente. Escriba help para ver la lista de comandos" << endl;
      }
   }
   return 0;
}
