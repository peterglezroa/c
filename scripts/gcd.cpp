#include <stdio.h>
#include <algorithm>
#include <iostream>
using namespace std;
long gdc(long a, long b){
	long r;
	while(1){
		if(a >= b){
			r = a%b;
			if(r == 0)
				return b;
			a = b;
			b = r;
		}else{
			r = b%a;
			if(r == 0)
				return a;
			b = a;
			a = r;
		}
	}
}

int main(){
	long a, b, r;
	cout << "Give me both numbers: ";
	cin >> a >> b;
	cout << gdc(a,b)<<endl;	
return 0;
}
