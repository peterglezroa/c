#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <string>
using namespace std;
struct item{
	long weight;
	long value;
};
int main(){
	int n, w, i, j;
	cout << "How many objects? ";
	cin >> n;
	cout << "How much can you carry? ";
	cin >> w;
	w++;
	long dp[n][w]; //columns - your weigth | rows - items
	item items[n];
	
	for(i = 0; i < n; i++){
		for(j = 0; j < w; j++){
			dp[i][j] = 0;
		}
	}

	cout << "Give me the weight of the items (Sorted!!): ";
	for(i = 0; i < n; i++){
		cin >> items[i].weight; 
	}
	cout << "Give me the value of the items: ";
	for(i = 0; i < n; i++){
		cin >> items[i].value;
	}
	for(i = 0; i < n; i++){
		cout << items[i].weight << "/" << items[i].value << " ";
	}
	cout << endl;
	for(i = 0; i < n; i++){
		for(j=0; j<w; j++){
			if(j >= items[i].weight){
				if(i>0)
					dp[i][j] = max(items[i].value + dp[i-1][j-items[i].weight], dp[i-1][j]);
				else
					dp[i][j] = items[i].value;
			}else{
				(i > 0)?dp[i][j] = dp[i-1][j]:0;
			}
			cout << dp[i][j] << " ";
		}
		cout << endl;
	}
	cout << "Max value to carry: " << dp[n][w];
	i = n-1; j = w-1;
    cout << "Take items: ";
	while(i>0){
		if(dp[i][j]!=dp[i-1][j]){
			cout << items[i].weight << "/" << items[i].value << " ";
			j-=items[i].weight;
		}
		i--;
	}
	cout << endl;	
	return 0;
}
